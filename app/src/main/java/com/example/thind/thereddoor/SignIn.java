package com.example.thind.thereddoor;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class SignIn extends AppCompatActivity implements View.OnClickListener {
    TextView notRegistered;
    ImageButton back;
    Button signin,forgot;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        notRegistered=(TextView)findViewById(R.id.notRegistered);
        signin=(Button)findViewById(R.id.signInButton);
        forgot=(Button)findViewById(R.id.forgot);
        back=(ImageButton)findViewById(R.id.back);
        back.setOnClickListener(this);
//            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
//            @Override
//            public void onClick(View v) {
//                finishAffinity();
//            }
//        });
        String text = "<font color=#ABACAD>Not Registered yet? </font>" +
                " <font color=#CD2E44><u>SIGN UP</u></font>" +
                "<font color=#ABACAD> here</font>";
        notRegistered.setText(Html.fromHtml(text));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sign_in, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.back:
                finishAffinity();
                break;
            case R.id.signInButton:
                break;
            case R.id.forgot:
                break;
            case R.id.notRegistered:
                break;
        }
    }
}
